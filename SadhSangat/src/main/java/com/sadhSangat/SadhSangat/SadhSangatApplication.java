package com.sadhSangat.SadhSangat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SadhSangatApplication {

	public static void main(String[] args) {
		SpringApplication.run(SadhSangatApplication.class, args);
	}
}
